package cs102.week04;

import java.util.ArrayList;

public class Library {
    private String libraryName;
    private int phoneNumber;
    private ArrayList<Book> bookCollection;

    public Library(String libraryName, int phoneNumber) {
        this.libraryName = libraryName;
        this.phoneNumber = phoneNumber;
        this.bookCollection = new ArrayList<Book>();
    }

    public String getLibraryName() {
        return this.libraryName;
    }

    public int getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void addBook(Book book) {
        this.bookCollection.add(book);
    }

    public boolean contains(String bookTitle) {
        Book book;
        for (int i = 0; i < this.bookCollection.size(); ++i) {
            book = this.bookCollection.get(i);
            if (book.getTitle().equals(bookTitle)) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        String s = "Library Name: " + this.libraryName + "\n"
                + "---" + "\n";
        for (int i = 0; i < this.bookCollection.size(); ++i) {
            s += "Book " + (i + 1) + ":\n" + this.bookCollection.get(i).toString() + "\n";
        }
        return s;
    }
}
