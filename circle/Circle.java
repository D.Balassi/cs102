package cs102.week01;

public class Circle {
    double x;
    double y;
    double r;

    public Circle(double ix, double iy, double ir) {
        x = ix;
        y = iy;
        r = ir;
    }

    public Circle(double ir) {
        x = 0.0;
        y = 0.0;
        r = ir;
    }


    public double getArea() {
        return Math.PI*Math.pow(r, 2);
    }
    public double getCircumference() {
        return Math.PI*2*r;
    }
}
