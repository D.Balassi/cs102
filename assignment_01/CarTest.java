package cs102.week02;

public class CarTest {

    public static void main(String[] args) {
        Car myFirstCar = new Car("Mercedes", "Red");
        Car mySecondCar = new Car("Toyota", "White");
        Car myThirdCar = new Car("Peugeot", "Blue");

        myFirstCar.display();
        mySecondCar.display();
        myThirdCar.display();

        myFirstCar.drive(10, 90);
        myFirstCar.incrementGear();
        myFirstCar.setGear(8);
        mySecondCar.decrementGear();
        mySecondCar.setGear(2);
        mySecondCar.setColor("Pink");
        myThirdCar.setGear(5);
        myThirdCar.incrementGear();
        myThirdCar.decrementGear();
        myThirdCar.drive(4, 120);

        System.out.println("----");
        myFirstCar.display();
        mySecondCar.display();
        myThirdCar.display();
    }
}

